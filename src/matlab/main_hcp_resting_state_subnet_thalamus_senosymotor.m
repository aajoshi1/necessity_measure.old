%||AUM||
clc; clear all; close all;
addpath(genpath('\home\ajoshi\git_sandbox\necessity_measure\src'));
addpath(genpath('\home\ajoshi\git_sandbox\svreg-matlab\src'));
addpath(genpath('\home\ajoshi\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
%ll=dir('D:\hcp\');
 
load vrest_roiwise_allsubs_signal
clear vrest_roiwise3
load labs
load v
%labs_id=[18,19,64,137,47,120,48,121];
%labs_id=[18,64,47,48];
labs_id=[19,137,120,121];
for kk=1:length(vrest_roiwise_allsub)
    all_t_ser=abs(vrest_roiwise_allsub{kk}(labs_id,:));
    %vrest_roiwise2=(all_t_ser./repmat(std(all_t_ser')',1,length(all_t_ser)));
    vrest_roiwise2=all_t_ser./std(all_t_ser(:));
    vrest_roiwise_pval{kk}=1-(1-normcdf(vrest_roiwise2,0,1));
end

%20=THALAMUS_LEFT
%21=THALAMUS_RIGHT
%1046=S_central Left
%1046+75=S_central Right
%1028  G_postcentral Left                   
%1028+75  G_postcentral Right 
%1029  G_precentral Left         
%1029+75  G_precentral Right       


kk=0;
%vrest_roiwise3_all=vrest_roiwise3;

for kk1=1:length(vrest_roiwise_pval)
    kk=kk1;
    vrest_roiwise3=vrest_roiwise_pval{kk};
    tic
    for l1=1:length(labs_id)
        vrest_roiwise3_l1=vrest_roiwise3((l1),:);
        parfor l2=1:length(labs_id)
           
            remroi=setdiff(1:length(labs_id),[l1,l2]);
            N(l1,l2)=necessity_empirical_kde(vrest_roiwise3_l1',vrest_roiwise3((l2),:)');
            Np(l1,l2)=necessity_empirical_kde_partial(vrest_roiwise3_l1',vrest_roiwise3((l2),:)',vrest_roiwise3(remroi,:)');
            [N_XY, N_YX]=condest_fast_kde(vrest_roiwise3_l1',vrest_roiwise3((l2),:)');            
            cond=0;
            if abs(N_XY)<abs(N_YX)
                if N_YX>0
                    %fprintf('Increase in X implies increase in Y: X==>Y\n');    
                    cond=1;
                else
                    %fprintf('Increase in X implies decrease in Y: X==>Y\n');    
                    cond=2;
                end
            else
                if N_XY>0
                    %fprintf('Increase in Y implies increase in X: Y==>X\n');   
                    cond=3;
                else
                    %fprintf('Increase in Y implies decrease in X: Y==>X\n');    
                    cond=4;
                end
            end
            CondEnt(l1,l2)=cond;
            N_XY_all(l1,l2)=N_XY;
%            hXY_all{l1,l2}=hXY;
            [C_XY]=igci(vrest_roiwise3_l1,vrest_roiwise3((l2),:)',1,1);
            igci_C_XY_all(l1,l2)=C_XY;%igci_diff_p(l1,l2)=C_XY_diff_p;
            %l2
        end
      %  l1
      %  toc
    end    
    %figure;imagesc(N);
    Necessity{kk}=N;    Necessity_partial{kk}=Np;

    igci_C_XY_all_1{kk}=igci_C_XY_all;
    %igci_diff_p_all{kk}=igci_diff_p;
    N_XY_all_subs{kk}=N_XY_all;
    CondEnt_subs{kk}=CondEnt;
%    hXY_all_subs{kk}=hXY_all;
    fprintf('subject %d is done\n',kk);drawnow;
  %  save(sprintf('Necessity_all6_%d',kk));
end
    save Necessity6_subnet_thalamus_somatosensory_right_2_kde 


N=0*Necessity{1};
for jj=1:length(Necessity)
    Ne(:,:,jj)=(Necessity{jj});
end
NN=trimmean(Ne,10,3);
figure;imagesc(NN);title('Necessity');

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((Ne(jj,kk,:)));
    end
end

figure;imagesc(NN.*(NNp<0.05));title('Necessity pval');





Np=0*Necessity_partial{1};
for jj=1:length(Necessity_partial)
    Ne(:,:,jj)=(Necessity_partial{jj});
end
NNp=trimmean(Ne,10,3);
figure;imagesc(NNp);title('partial necessity');

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp1(jj,kk)]=ttest((Ne(jj,kk,:)));
    end
end

figure;imagesc(NNp.*(NNp1<0.05));title('partial necessity pval');




for jj=1:length(N_XY_all_subs)
    ENe(:,:,jj)=abs(N_XY_all_subs{jj});
end
ENN=trimmean(ENe,10,3);

figure;imagesc(ENN-diag(diag(ENN)));title('Entropy based');
for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((ENe(jj,kk,:)));
    end
end

figure;imagesc(ENN.*(NNp<0.05));title('Entropy based pval');



for jj=1:length(igci_C_XY_all_1)
    ENeigci(:,:,jj)=(igci_C_XY_all_1{jj});
end
ENNigci=trimmean(ENeigci,10,3);
figure;imagesc(ENNigci);title('IGCI');


for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((ENeigci(jj,kk,:)));
    end
end

figure;imagesc(ENNigci.*(NNp<0.05));title('IGCI pval');


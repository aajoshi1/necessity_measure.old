clc;clear;close all;
addpath(genpath('.'));
a = [1; 2; 3];
x = [];
for i = 1:100000
  x(:, i) = dirichlet_sample(a);
end

figure(1);
plot(x(1, :), x(2, :), 'o');% axis([0,1 0,1]);

N12=necessity_empirical(x(1, :),x(2, :))
N21=necessity_empirical(x(2, :),x(1, :))

for jj=1:3
    figure;
    hist(x(jj, :),100);
end
fprintf('Exact    Empirical\n');



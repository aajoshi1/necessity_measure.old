%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
load Necessity5_only


ll=dir('D:\hcp\');
jj=1;
for kk=1:length(ll)
    if ll(kk).isdir==1 && length(ll(kk).name)==6
        snames{jj}=ll(kk).name;jj=jj+1;
    end
end

kk=0;
for kk1=1:1
    subname=snames{kk1};
    if ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname),'file') || ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname),'file')
        continue;
    end
    kk=kk+1;
    v=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname));
    vs=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname));
end
    v.brainstructure(1:length(vs.aparc_a2009s))=1000+vs.aparc_a2009s;  %adding 1000 to distinguish surface labels from volume labels
    labs=unique(v.brainstructure(~isnan(v.brainstructure)));
    labs=setdiff(labs,[1000,1076]);
    

%N=0*Necessity{1};
for jj=1:length(Necessity)
    Ne(:,:,jj)=(Necessity{jj});
end

rois=[5,6,25,98,66,139];

NN=trimmean(Ne,5,3);
figure;imagesc(NN(rois,rois));
for kk=1:size(Ne,2)
for jj=1:size(Ne,1)
    [NNt(jj,kk),NNp(jj,kk)]=ttest((Ne(jj,kk,:)-Ne(kk,jj,:)));
end
end

figure;imagesc((NN-NN')>0);


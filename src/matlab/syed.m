function [theoretical, numerical, empirical, formulaic] = syed()

N = 10000; nBins = 2^8;
addpath('~/Desktop/Dropbox/bst_connectivity/utilities/')

theoretical = zeros(17, 9, 17);
numerical   = zeros(17, 9, 17);
empirical   = zeros(17, 9, 17);
formulaic   = zeros(17, 9, 17);

fprintf(1, 'Begin ... '); start = tic;
for a = 2:0.5:10
for b = 2:10
for c = 2:0.5:10
      
  %% Simulate bivariate Dirichlet beta
  % a = 4; b = 2; c = 2;
  samples = gamrnd(repmat([a b c], N, 1), 1, N, 3)';
  samples = samples ./ repmat(sum(samples), 3, 1);
  X = samples(1,:); Y = samples(2,:);

  %% Parameters of density estimation: locations of values and sample space
  x = linspace(0, 1, nBins+1); dx = 1/nBins; x = x(1:(end-1)) + dx/2;
  y = linspace(0, 1, nBins+1); dy = 1/nBins; y = y(1:(end-1)) + dy/2;
  meshX = x' * ones(1, nBins); meshY = ones(nBins, 1) * y;
  support = (meshX + meshY) < 1; supportX = meshX(support); supportY = meshY(support);
  hX = 1.06 * std(X) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(X), iqr(X)/1.34) * N^(-1/5)
  hY = 1.06 * std(Y) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(Y), iqr(Y)/1.34) * N^(-1/5)

  %% Necessity options

  % Closed-form PDFs
  [theoretical(round(a*2-3),b-1,round(c*2-3)), ~, marginalTheoretical, conditionalTheoretical, integrandTheoretical] = ...
    bst_necessity_theoretical(a, b, c, x, y, supportX, supportY, nBins, support, dx, dy);

  % Numerical integration of closed-form joint PDF
  [numerical(round(a*2-3),b-1,round(c*2-3)), ~, marginalNumerical, conditionalNumerical, integrandNumerical] = ...
    bst_necessity_numerical(a, b, c, x, y, supportX, supportY, nBins, support, dx, dy);

  % Kernel density estimation
  [empirical(round(a*2-3),b-1,round(c*2-3)), ~, marginal, conditional, integrand] = ...
    bst_necessity(X, Y, hX, hY, nBins, support);

  % Formulaic necessity
  formulaic(round(a*2-3),b-1,round(c*2-3)) = ...
    1/24 * (a + 3 * c - 2 / beta(a,c)) - 1/6; toc(start) * 17 * 9 * 17 / 3600

  %% Plot comparisons

  % figure(1); clf(1);
  % subplot(311);
  % plot(y, marginalTheoretical, 'k'); title('Theoretical marginal distribution'); xlabel('source'); ylabel('probability distribution'); axis([0 1 0 7]);
  % hold on;
  % plot(y, marginalNumerical, 'r--'); title('Numerical marginal distribution'); xlabel('source'); ylabel('probability distribution'); axis([0 1 0 7]);
  % hold off;
  % subplot(323); imagesc(x, y, log(conditionalTheoretical)); axis xy; colorbar; title('Theoretical conditional distribution'); xlabel('sink'); ylabel('source');
  % subplot(324); imagesc(x, y, log(conditionalNumerical)); axis xy; colorbar; title('Numerical conditional distribution'); xlabel('sink'); ylabel('source');
  % subplot(325); imagesc(x, y, integrandTheoretical); axis xy; colorbar; title(['Theoretical necessity: ' num2str(theoretical)]); xlabel('sink'); ylabel('source');
  % subplot(326); imagesc(x, y, integrandNumerical); axis xy; colorbar; title(['Numerical necessity: ' num2str(numerical)]); xlabel('sink'); ylabel('source');

  % figure(2); clf(2);
  % subplot(311);
  % plot(y, marginalTheoretical, 'k'); title('Theoretical marginal distribution'); xlabel('source'); ylabel('probability distribution'); axis([0 1 0 7]);
  % hold on;
  % plot(y, marginal, 'r--'); title('Numerical marginal distribution'); xlabel('source'); ylabel('probability distribution'); axis([0 1 0 7]);
  % hold off;
  % subplot(323); imagesc(x, y, log(conditionalTheoretical)); axis xy; colorbar; title('Theoretical conditional distribution'); xlabel('sink'); ylabel('source');
  % subplot(324); imagesc(x, y, log(conditional)); axis xy; colorbar; title('Empirical conditional distribution'); xlabel('sink'); ylabel('source');
  % subplot(325); imagesc(x, y, integrandTheoretical); axis xy; colorbar; title(['Theoretical necessity: ' num2str(theoretical)]); xlabel('sink'); ylabel('source');
  % subplot(326); imagesc(x, y, integrand); axis xy; colorbar; title(['Empirical necessity: ' num2str(empirical)]); xlabel('sink'); ylabel('source');

end
end
fprintf(1, '.');
end
disp(['done in ' num2str(toc(start)) ' seconds.']);

figure(3); clf(3); set(3, 'Name', 'Theoretical');
for b = 2:10
  subplot(3, 3, b-1);
  imagesc(2:0.5:10, 2:0.5:10, squeeze(theoretical(:, b-1, :))); axis xy; axis square; colorbar;
  title(['b = ' int2str(b)]); xlabel('c'); ylabel('a');
end

figure(4); clf(4); set(4, 'Name', 'Numerical');
for b = 2:10
  subplot(3, 3, b-1);
  imagesc(2:0.5:10, 2:0.5:10, squeeze(numerical(:, b-1, :))); axis xy; axis square; colorbar;
  title(['b = ' int2str(b)]); xlabel('c'); ylabel('a');
end

figure(5); clf(5); set(5, 'Name', 'Empirical');
for b = 2:10
  subplot(3, 3, b-1);
  imagesc(2:0.5:10, 2:0.5:10, squeeze(empirical(:, b-1, :))); axis xy; axis square; colorbar;
  title(['b = ' int2str(b)]); xlabel('c'); ylabel('a');
end

figure(6); clf(6); set(6, 'Name', 'Formulaic');
for b = 2:10
  subplot(3, 3, b-1);
  imagesc(2:0.5:10, 2:0.5:10, squeeze(formulaic(:, b-1, :))); axis xy; axis square; colorbar;
  title(['b = ' int2str(b)]); xlabel('c'); ylabel('a');
end

end

%% ======================================================================== THEORETICAL ========================================================================
function [necessity, joint, marginal, conditional, integrand] = bst_necessity_theoretical(a, b, c, x, y, supportX, supportY, nBins, support, dx, dy)

%% Joint distribution
joint = zeros(nBins, nBins);
joint(support) = ...
  gamma(a + b + c) / (gamma(a) * gamma(b) * gamma(c)) ...
  * (supportX .^ (a-1)) ...
  .* (supportY .^ (b-1)) ...
  .* ((1 - supportX - supportY) .^ (c-1));

%% Marginal distribution of source
marginal = zeros(1, nBins);
marginal(1:(end-1)) = gamma(a+b+c)/(gamma(b)*gamma(a+c)) * y(1:(end-1)).^(b-1) .* (1-y(1:(end-1))).^(a+c-1);

%% Conditional distribution of sink given source
conditional = zeros(nBins, nBins);
conditional(support) = ...
  gamma(a + c) / (gamma(a) * gamma(c)) ...
  * ((supportX ./ (1 - supportY)) .^ (a-1)) ...
  .* ((1 - (supportX ./ (1 - supportY))) .^ (c-1)) ...
  ./ (1 - supportY);
conditional(isinf(conditional)) = 0;

%% Theoretical necessity
integrand = zeros(nBins, nBins);
for m = 1:nBins
  for n = 1:nBins
    if support(m,n) && conditional(m,n) ~= 0 && ~isnan(conditional(m,n)) && ~isinf(conditional(m,n))
      integrand(m, n) = x(m) * (2 * y(n) - 1) * log(conditional(m,n));
    end
  end
end
necessity = sum(sum(integrand)) * dx * dy;

end

%% ========================================================================= NUMERICAL =========================================================================
function [necessity, joint, marginal, conditional, integrand] = bst_necessity_numerical(a, b, c, x, y, supportX, supportY, nBins, support, dx, dy)
  
%% Joint distribution
joint = zeros(nBins, nBins);
joint(support) = ...
  gamma(a + b + c) / (gamma(a) * gamma(b) * gamma(c)) ...
  * (supportX .^ (a-1)) ...
  .* (supportY .^ (b-1)) ...
  .* ((1 - supportX - supportY) .^ (c-1));

%% Marginal distribution of source
marginal = trapz(x, joint);

%% Conditional distribution of sink given source
conditional = zeros(nBins, nBins);
replicated = repmat(marginal, nBins, 1);
conditional(support) = joint(support) ./ replicated(support);
conditional(isinf(conditional)) = 0;

%% Necessity
integrand = zeros(nBins, nBins);
for m = 1:nBins
  for n = 1:nBins
    if support(m,n) && conditional(m,n) ~= 0 && ~isnan(conditional(m,n)) && ~isinf(conditional(m,n))
      integrand(m, n) = x(m) * (2 * y(n) - 1) * log(conditional(m,n));
    end
  end
end
necessity = sum(sum(integrand)) * dx * dy;

end

%% ========================================================================= EMPIRICAL =========================================================================
% function [necessity, joint, marginal, conditional, integrand] = bst_necessity(X, Y, hX, hY, nBins, support)
% 
% %% Joint distribution
% [joint, bins] = bst_pdf([X; Y], [hX; hY], [0 1; 0 1], nBins);
% x = bins(1, :); y = bins(2, :);
% dx = mean(diff(x)); dy =  mean(diff(y));
% 
% %% Marginal distribution of source
% marginal = bst_pdf(Y, hY, [0 1], nBins);
% 
% %% Conditional distribution of sink given source
% conditional = zeros(nBins, nBins);
% replicated = repmat(marginal, nBins, 1);
% conditional(support) = joint(support) ./ replicated(support);
% conditional(isinf(conditional)) = 0;
% 
% %% Necessity
% integrand = zeros(nBins, nBins);
% for m = 1:nBins
%   for n = 1:nBins
%     if support(m,n) && conditional(m,n) ~= 0 && ~isnan(conditional(m,n)) && ~isinf(conditional(m,n))
%       integrand(m, n) = x(m) * (2 * y(n) - 1) * log(conditional(m,n));
%     end
%   end
% end
% necessity = sum(sum(integrand)) * dx * dy;
% 
% end

%% =========================================================================== trash ===========================================================================
% %% Bin sizes for marginal distributions of Dirichlet variables
% N = 10000;
% addpath('~/Desktop/Dropbox/toolboxes/')
% 
% %% Simulate bivariate Dirichlet beta
% a = 2; b = 9; c = 2;
% 
% figure(1); clf(1); axis([0 1 0 4]);
% for nBins = 2.^(4:8)
%   %% Parameters of density estimation: locations of values and sample space
%   locationsX = linspace(0, 1, nBins+1); dx = 1/nBins; locationsX = locationsX(1:(end-1)) + dx/2;
%   locationsY = linspace(0, 1, nBins+1); dy = 1/nBins; locationsY = locationsY(1:(end-1)) + dy/2;
%   binsX = locationsX' * ones(1, nBins);
%   binsY = ones(nBins, 1) * locationsY;
%   support = (binsX + binsY) < 1;
%   supportX = binsX(support); supportY = binsY(support);
%   hX = 1.06 * std(X) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(X), iqr(X)/1.34) * N^(-1/5)
%   hY = 1.06 * std(Y) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(Y), iqr(Y)/1.34) * N^(-1/5)
% 
%   %% Distributions
% 
%   % Joint distribution
%   jointTheoretical = zeros(nBins, nBins);
%   jointTheoretical(support) = ...
%     gamma(a + b + c) / (gamma(a) * gamma(b) * gamma(c)) ...
%     * (supportX .^ (a-1)) ...
%     .* (supportY .^ (b-1)) ...
%     .* ((1 - supportX - supportY) .^ (c-1));
% 
%   % Theoretical distribution
%   marginalTheoretical = gamma(a+b+c)/(gamma(b)*gamma(a+c)) * locationsY.^(b-1) .* (1-locationsY).^(a+c-1);
% 
%   % Numerical distribution
%   marginalNumerical = trapz(locationsX, jointTheoretical);
%   
%   %% Plot
%   hold on, plot(locationsY, marginalTheoretical, 'k-'), hold off;
%   hold on, plot(locationsY, marginalNumerical, 'r--'), hold off;
% end
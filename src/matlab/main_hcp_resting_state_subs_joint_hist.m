%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
ll=dir('D:\hcp\');
 
load vrest_roiwise3_subs
load labs
load v
labs_id=[3,4,66,139,25,98];
NB=200;%eps=.1;
%X=X+rand(size(X))*eps;
%Z=Z+rand(size(Z))*eps;
%xedges=linspace(min(X(:)-eps),max(X(:)+eps),NB+1);
%zedges=linspace(min(Z(:)-eps),max(Z(:)+eps),NB+1);

xedges=linspace(0-eps,1+eps,NB+1);
zedges=linspace(0-eps,1+eps,NB+1);

kk=0;
vrest_roiwise3_all=vrest_roiwise3;

for kk1=1:length(vrest_roiwise3_all)
    kk=kk1;
    vrest_roiwise3=vrest_roiwise3_all{kk};
    for l1=1:length(labs_id)
        vrest_roiwise3_l1=vrest_roiwise3(labs_id(l1),:);
        for l2=1:length(labs_id)
           vrest_roiwise3_l2=vrest_roiwise3(labs_id(l2),:);
           hXZ = mutual_histogram_parzen_variable_size_multithread_double(vrest_roiwise3_l1, vrest_roiwise3_l2, xedges(1), xedges(end), NB, round(NB/3), 4);
        end
    end
end

%||AUM||
%||Shree Ganeshaya Namaha||
function [N_XY, N_YX, N_XY_diff_p,hXY]= condest_pval_new2(X_p,Y_p)

[~,hXY,EX,EZ,N_XY]=conditional_entropy(X_p,Y_p);N_XY=N_XY./EZ;
    [~,hXY1,EX,EZ,N_YX]=conditional_entropy(Y_p,X_p);N_YX=N_YX./EZ;
    
    N_XY_null=zeros(1000,1);N_YX_null=zeros(1000,1);
    for jj=1:1000
        Vec=[X_p',Y_p'];
        aa=1+round(rand(length(X_p),1));
        ind1=sub2ind(size(Vec),[1:length(aa)]',aa);
        ind2=sub2ind(size(Vec),[1:length(aa)]',3-aa);
        Vecp=[Vec(ind1),Vec(ind2)];
        [~,~,EX,EZ,N_XY_null(jj)]=conditional_entropy(Vecp(:,1),Vecp(:,2));N_XY_null(jj)=N_XY_null(jj)./EZ;
        [~,~,EX,EZ,N_YX_null(jj)]=conditional_entropy(Vecp(:,2),Vecp(:,1));N_YX_null(jj)=N_YX_null(jj)./EZ;
    end
    N_XY_diff_p=sum((N_XY_null-N_YX_null)>(N_XY-N_YX))/1000;
    N_YX_diff_p=sum((N_YX_null-N_XY_null)>(N_YX-N_XY))/1000;

%||AUM||
clc; clear all; close all;
addpath(genpath('/home/ajoshi/git_sandbox/necessity_measure/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg-matlab/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg-matlab/dev/thickness/'));
%vpvc_lab_5min=load_nii_z('C:/Users/ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
%ll=dir('D:\hcp\');
 
load vrest_roiwise_allsubs_signal
clear vrest_roiwise3
load labs
load v
labs_idl=[];[28,29,44,25,35];%left DMN
labs_id=[labs_idl,101,102,117,118,108];%right DMN

for kk=1:length(vrest_roiwise_allsub)
    all_t_ser=abs(vrest_roiwise_allsub{kk}(labs_id,:));
    %vrest_roiwise2=(all_t_ser./repmat(std(all_t_ser')',1,length(all_t_ser)));
    vrest_roiwise2=all_t_ser./std(all_t_ser(:));
    vrest_roiwise_pval{kk}=1-(1-normcdf(vrest_roiwise2,0,1));
end


kk=1;
%vrest_roiwise3_all=vrest_roiwise3;

%for kk1=1:length(vrest_roiwise_pval)
    %kk=kk1;
    vrest_roiwise3=vrest_roiwise_pval{kk};
    %tic
    l1=1;
    l2=3;
    
        X=vrest_roiwise3(l1,:)';
       % Y=vrest_roiwise3(l2,:)';
        Y=max(X,(rand(size(X))>.8));
        %Z=max(X,Y);
        Z=max(Y,(rand(size(X))>.8));

        N_xy=necessity_empirical_kde(X,Y);
        N_xz=necessity_empirical_kde(X,Z);
        N_yz=necessity_empirical_kde(Y,Z);

        N_yx=necessity_empirical_kde(Y,X);
        N_zx=necessity_empirical_kde(Z,X);
        N_zy=necessity_empirical_kde(Z,Y);
   
        
        pN_xy=necessity_empirical_kde_partial2(X,Y,Z);
        pN_xz=necessity_empirical_kde_partial2(X,Z,Y);
        pN_yz=necessity_empirical_kde_partial2(Y,Z,X);
        
        
        pN_yx=necessity_empirical_kde_partial2(Y,X,Z);
        pN_zx=necessity_empirical_kde_partial2(Z,X,Y);
        pN_zy=necessity_empirical_kde_partial2(Z,Y,X);

       n(2,1)= N_yx-N_xy
       n(3,1)= N_zx-N_xz
       n(3,2)= N_zy-N_yz
        
pn(2,1)=pN_yx-pN_xy
pn(3,1)=pN_zx-pN_xz
pn(3,2)=pN_zy-pN_yz
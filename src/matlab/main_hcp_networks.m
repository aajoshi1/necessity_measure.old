%||AUM||
clc; clear all; close all;
addpath(genpath('/home/ajoshi/git_sandbox/necessity_measure/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg-matlab/src'));
addpath(genpath('/home/ajoshi/git_sandbox/svreg-matlab/dev/thickness/'));
%vpvc_lab_5min=load_nii_z('C:/Users/ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
%ll=dir('D:\hcp\');
 
load vrest_roiwise_allsubs_signal
clear vrest_roiwise3
load labs
load v
%labs_id=[18,19,64,137,47,120,48,121];
%labs_id=[18,64,47,48];% Thalamus Sensory motor left
labs_id=[19,137,120,121];% Thalamus Sensory motor right
%labs_id=[3,4,66,139,25,98];
%labs_id=[139,25,98];% Right Amyg, Ant Ins Ctx, G_and_S_cingul-Ant Left
%labs_id=[28,29,44,45,35];%labs=[1009,1010,1025,1026, 1016];% left hemi
%default mode network
%labs_id=[28,29,44,45,35];%left DMN
%labs_id=[101,102,117,118,108];%right DMN
%labs_id=[102,118,108];%right DMN
%labs=[5,6,1048,1048+75,66,139];


for kk=1:length(vrest_roiwise_allsub)
    all_t_ser=abs(vrest_roiwise_allsub{kk}(labs_id,:));
    %vrest_roiwise2=(all_t_ser./repmat(std(all_t_ser')',1,length(all_t_ser)));
    vrest_roiwise2=all_t_ser./std(all_t_ser(:));
    vrest_roiwise_pval{kk}=1-(1-normcdf(vrest_roiwise2,0,1));
end
%5=AMYGDALA_LEFT
%6=AMYGDALA_RIGHT
%1048=ANTERIOS INSULAR CORTEX Left
%1048+75=ANTERIOS INSULAR CORTEX Right
%1006=G_and_S_cingul-Ant Left
%1006+75=G_and_S_cingul-Ant Right
%20=THALAMUS_LEFT
%21=THALAMUS_RIGHT
%1046=S_central Left
%1046+75=S_central Right
%1028  G_postcentral Left                   
%1028+75  G_postcentral Right 
%1029  G_precentral Left         
%1029+75  G_precentral Right

%%%DMN%%%%
%1009   G_cingul-Post-dorsal Left
%1009+75  G_cingul-Post-dorsal Right
%1010   G_cingul-Post-ventral Left
%1010+75    G_cingul-Post-ventral Right
%1025    G_pariet_inf-Angular Left
%1025+75   G_pariet_inf-Angular Right
%1006       G_and_S_cingul-Ant Left
%1006+75   G_and_S_cingul-Ant Right
%1016       G_front_sup Left
%1016+75    G_front_sup Right




%20=THALAMUS_LEFT
%21=THALAMUS_RIGHT
%1046=S_central Left
%1046+75=S_central Right
%1028  G_postcentral Left                   
%1028+75  G_postcentral Right 
%1029  G_precentral Left         
%1029+75  G_precentral Right       


kk=0;
%vrest_roiwise3_all=vrest_roiwise3;

for kk1=1:length(vrest_roiwise_pval)
    kk=kk1;
    vrest_roiwise3=vrest_roiwise_pval{kk};
    tic
    for l1=1:length(labs_id)
        vrest_roiwise3_l1=vrest_roiwise3((l1),:);
        parfor l2=1:length(labs_id)
           
            remroi=setdiff(1:length(labs_id),[l1,l2]);
            N(l1,l2)=necessity_empirical(vrest_roiwise3_l1',vrest_roiwise3((l2),:)');
        %    Np(l1,l2)=necessity_empirical_kde_partial(vrest_roiwise3_l1',vrest_roiwise3((l2),:)',vrest_roiwise3(remroi,:)');
        %    [N_XY, N_YX]=condest_fast_kde(vrest_roiwise3_l1',vrest_roiwise3((l2),:)'); 
        %%N_XY is conditional entropy
        %    cond=0;
        %    if abs(N_XY)<abs(N_YX)
        %        if N_YX>0
                    %fprintf('Increase in X implies increase in Y: X==>Y\n');    
        %            cond=1;
        %        else
                    %fprintf('Increase in X implies decrease in Y: X==>Y\n');    
        %            cond=2;
        %        end
        %    else
        %        if N_XY>0
                    %fprintf('Increase in Y implies increase in X: Y==>X\n');   
        %            cond=3;
        %        else
                    %fprintf('Increase in Y implies decrease in X: Y==>X\n');    
        %            cond=4;
        %        end
  %          end
        %    CondEnt(l1,l2)=cond;
            %N_XY_all(l1,l2)=N_XY;
%            hXY_all{l1,l2}=hXY;
         %   [C_XY]=igci(vrest_roiwise3_l1,vrest_roiwise3((l2),:)',1,1);
         %   igci_C_XY_all(l1,l2)=C_XY;%igci_diff_p(l1,l2)=C_XY_diff_p;
            %l2
        end
      %  l1
      %  toc
    end    
    %figure;imagesc(N);
    Necessity{kk}=N;    
    %Necessity_partial{kk}=Np;
    Corr{kk}=corr(vrest_roiwise3');
    Par_Corr{kk} = partialcorr(vrest_roiwise3');
%    igci_C_XY_all_1{kk}=igci_C_XY_all;
    %igci_diff_p_all{kk}=igci_diff_p;
    %N_XY_all_subs{kk}=N_XY_all;
 %   CondEnt_subs{kk}=CondEnt;
%    hXY_all_subs{kk}=hXY_all;
    fprintf('subject %d is done\n',kk);drawnow;
  %  save(sprintf('Necessity_all6_%d',kk));
end
   % save Necessity6_subnet_thalamus_somatosensory_right_2_kde 
    %save Necessity6_subnet_amyg_ant_ins_cing_right_2_kde 
    save Necc_Corr_Thalamus_Network_right_nopartial

N=0*Necessity{1};
for jj=1:length(Necessity)
    Ne(:,:,jj)=(Necessity{jj});
end
NN=trimmean(Ne,10,3);
figure;imagesc(NN);title('Necessity'); colormap gray;

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNp(jj,kk),NNt(jj,kk)]=ranksum1side(squeeze(Ne(jj,kk,:)),squeeze(Ne(kk,jj,:)));
        %[NNt(jj,kk),NNp(jj,kk)]=ttest(Ne(jj,kk,:)-Ne(kk,jj,:));
    end
end

figure;imagesc(NN.*(NNp<0.05));title('Necessity pval');  colormap gray;

for jj=1:length(Corr)
    C(:,:,jj)=(Corr{jj});
end
mean_Corr=trimmean(C,10,3); 

figure;imagesc(mean_Corr); title('Correlation'); colormap gray;

for kk=1:size(C,2)
    for jj=1:size(C,1)
        [CCp(jj,kk),CCt(jj,kk)]=ranksum1side(squeeze(C(jj,kk,:)),0*squeeze(C(kk,jj,:)));
    end
end

figure;imagesc(mean_Corr.*(CCp<0.05));title('Correlation pval'); colormap gray;


for jj=1:length(Par_Corr)
    parC(:,:,jj)=(Par_Corr{jj});
end
mean_par_Corr=trimmean(parC,10,3); 

figure;imagesc(mean_par_Corr); title('Partial Correlation'); colormap gray;

for kk=1:size(C,2)
    for jj=1:size(C,1)
        [parCCp(jj,kk),parCCt(jj,kk)]=ranksum1side(squeeze(parC(jj,kk,:)),0*squeeze(parC(kk,jj,:)));
    end
end

figure;imagesc(mean_par_Corr.*(parCCp<0.05));title('Partial Correlation pval'); colormap gray;


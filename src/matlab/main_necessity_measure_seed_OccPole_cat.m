opengl software;clc;clear ;close all;
restoredefaultpath;
addpath(genpath('/home/ajoshi/sipi/my_functions'));
addpath(genpath('/home/ajoshi/SpiderOak/necessity_measure/src'));
%addpath(genpath('/home/ajoshi/fcon_1000/matlab_scripts/david_code'));
%addpath(genpath('../../uPC/Graphical-Lasso/'));

flst=dir('/home/ajoshi/fcon_1000/Beijing/sub*');
%gunzip('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii.gz')
v_seed=load_nii('/home/ajoshi/fcon_1000/matlab_scripts/HarvardOxford-cort-maxprob-thr25-3mm.nii');
%delete('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii');
sn=0;
    gunzip('/home/ajoshi/fcon_1000/scripts/templates/MNI152_T1_3mm_brain_mask.nii.gz','/tmp/tmpPCC');
    vmsk=load_nii('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    delete('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    vm=vmsk.img;clear vmsk;
%    vm=repmat(vm,[1,1,1,225]);
    maskind=find(vm);clear vm;load sz;
    Nvox2seed_sub=zeros(sz(1)*sz(2)*sz(3),length(flst));
    Nseed2vox_sub=zeros(sz(1)*sz(2)*sz(3),length(flst));
    
for subno=1:length(flst)
    
    if ~exist(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'file')
        continue;
    end
    sn=sn+1;
    
    gunzip(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'/tmp/tmpPCC');
    v_rest=load_nii('/tmp/tmpPCC/rest_res2standard.nii');
    delete('/tmp/tmpPCC/rest_res2standard.nii');
    
    %list_labels=unique(atlas_seg2.img(:));list_labels(list_labels==0)=[];list_labels(list_labels==50)=[];
    %r2 = reshape(rest_vol.img,61*73*61,size(rest_vol.img,4));
    %sz=size(v_rest.img);
    t_ser = zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));t_ser1=t_ser;
    seed_ind=find(v_seed.img(:)==48);
    %for vox_ind=1:sz(1)*sz(2)*sz(3)
        vrest_vox=zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));
        vrest_seed=zeros(1,size(v_rest.img,4));
        
        %vrest_vox=v_rest.img(maskind);
        
        for t=1:size(v_rest.img,4)
            v1=v_rest.img(:,:,:,t);
            vrest_vox(:,t)=v1(:);
            vrest_seed(t)=mean(v1(seed_ind));
        end
     %   vox_ind
        
        %[coeff,score]=princomp(vrest_roi,'econ');
       % t_ser(vox_ind,:)=mean(vox_ind);
        %  t_ser1(lab,:)=zscore(vrest_roi')';
        %   intd = find(atlas_seg2.img==list_labels(lab));
    %end
    %save t_set
    %C = partialcorr(t_ser(1:4:end,:)');figure;imagesc(C);
    t_ser2=zscore(vrest_vox,0,2);t_ser2_seed=zscore(vrest_seed,0,2);%clear t_ser;
    t_ser=1-(1-normcdf(t_ser2,0,1));t_ser_seed=1-(1-normcdf(t_ser2_seed,0,1));%t_ser=t_ser';
    
    clear Nvox2seed Nseed2vox Corrt;
    Nvox2seed=zeros(sz(1)*sz(2)*sz(3),1);
    Nseed2vox=zeros(sz(1)*sz(2)*sz(3),1);
    Corrt=zeros(sz(1)*sz(2)*sz(3),1);
    for jj=1:length(maskind)%sz(1)*sz(2)*sz(3)
            Nvox2seed(maskind(jj))=necessity_empirical(t_ser(maskind(jj),:),t_ser_seed);
            Nseed2vox(maskind(jj))=necessity_empirical(t_ser_seed,t_ser(maskind(jj),:));
            Corrt(maskind(jj))=corr(t_ser_seed',t_ser(maskind(jj),:)');
            %    N21(jj,kk)=necessity_empirical(t_ser(kk,:),t_ser(jj,:));
    end
    %figure;
    %imagesc(N12-N12');colorbar;
%    figure;
%    imagesc(N12);colorbar;caxis([-500,500]);
    
    Nvox2seed_sub(:,sn)=Nvox2seed;
    Nseed2vox_sub(:,sn)=Nseed2vox;
    Corr(:,sn)=Corrt;
    fprintf('%d/%d sub done\n',subno,length(flst));
end

save necessity_msr_seed_OccPole_corr_cat Nvox2seed_sub Nseed2vox_sub Corr

aaa=trimmean(Nvox2seed_sub,10,3);
NV=Nvox2seed_sub-repmat(aaa,[1,1,size(Nvox2seed_sub,3)]);
NV=trimmean(NV.^2,10,3);
avar=sqrt(NV);
aaa2=trimmean(Nseed2vox_sub,10,3);
NV2=Nvox2seed_sub-repmat(aaa2,[1,1,size(Nseed2vox_sub,3)]);
NV2=trimmean(NV2.^2,10,3);
avar2=sqrt(NV2);
%v_rest1=
%avar_v.img=avar;
vol=zeros(61,73,61);
vol(:)=trimmean(aaa,10,2);
%vol(:)=aaa(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_vox2OccPole.nii')
view_nii(nii2);
%avar=std(N12_sub,1,3);
vol=zeros(61,73,61);
vol(:)=trimmean(aaa2,10,2);
%vol(:)=aaa2(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_OccPole2vox.nii')
view_nii(nii2);
vol(:)=trimmean(aaa2,10,2)-trimmean(aaa,10,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_vox2OccPole_minus_OccPole2vox.nii')
view_nii(nii2);


vol=zeros(61,73,61);
vol(:)=trimmean(Corr,10,2);
%vol(:)=aaa2(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'Corr_OccPole2vox.nii')
view_nii(nii2);
% 
% figure;plot(aaa);
% figure;plot(aaa2);
% 
% figure;plot(abs(aaa-aaa2));
% figure;plot(avar);
% figure;plot(avar2);
% 
% 

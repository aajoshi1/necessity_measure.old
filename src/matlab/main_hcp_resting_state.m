%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
ll=dir('D:\hcp\');
 
jj=1;
for kk=1:length(ll)
    if ll(kk).isdir==1 && length(ll(kk).name)==6
        snames{jj}=ll(kk).name;jj=jj+1;
    end
end

kk=0;
for kk1=1:length(snames)
    subname=snames{kk1};
    if ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname),'file') || ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname),'file')
        continue;
    end
    kk=kk+1;
    v=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname));
    vs=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname));
    
    v.brainstructure(1:length(vs.aparc_a2009s))=1000+vs.aparc_a2009s;  %adding 1000 to distinguish surface labels from volume labels
    
    
    labs=unique(v.brainstructure(~isnan(v.brainstructure)));
    labs=setdiff(labs,[1000,1076]);
    
    for l=1:length(labs)
        l_ind=find(v.brainstructure==labs(l));
        for t=1:size(v.dtseries,2)
            v1=v.dtseries(:,t);
            vrest_roiwise(l,t)=mean(v1(l_ind));
        end
        %vrest_roiwise
        %l
    end
    vrest_roiwise2=vrest_roiwise-repmat(mean(vrest_roiwise,2),1,size(vrest_roiwise,2));
    vrest_roiwise2=vrest_roiwise2/std(vrest_roiwise2(:));
    vrest_roiwise3=1-(1-normcdf(vrest_roiwise2,0,1));
    % vrest_roiwise3(:)=vrest_roiwise3(randperm(length(vrest_roiwise3(:))));
    tic
    for l1=1:length(labs)
        vrest_roiwise3_l1=vrest_roiwise3(l1,:);
        parfor l2=1:length(labs)
           
            N(l1,l2)=necessity_empirical(vrest_roiwise3_l1,vrest_roiwise3(l2,:));
            [N_XY, N_YX, hXY]=condest_fast(vrest_roiwise3_l1,vrest_roiwise3(l2,:));            
            cond=0;
            if abs(N_XY)<abs(N_YX)
                if N_YX>0
                    %fprintf('Increase in X implies increase in Y: X==>Y\n');    
                    cond=1;
                else
                    %fprintf('Increase in X implies decrease in Y: X==>Y\n');    
                    cond=2;
                end
            else
                if N_XY>0
                    %fprintf('Increase in Y implies increase in X: Y==>X\n');   
                    cond=3;
                else
                    %fprintf('Increase in Y implies decrease in X: Y==>X\n');    
                    cond=4;
                end
            end
            CondEnt(l1,l2)=cond;
            %N_XY_diff_all(l1,l2)=N_XY_diff_p;
            hXY_all{l1,l2}=hXY;
            [C_XY]=igci(vrest_roiwise3_l1,vrest_roiwise3(l2,:),1,1);
            igci_C_XY_all(l1,l2)=C_XY;%igci_diff_p(l1,l2)=C_XY_diff_p;
            %l2
        end
        l1
        toc
    end    
    figure;imagesc(N);
    Necessity{kk}=N;
    igci_C_XY_all_1{kk}=igci_C_XY_all;
    igci_diff_p_all{kk}=igci_diff_p;
    N_XY_diff_all_subs{kk}=N_XY_diff_all;
    CondEnt_subs{kk}=CondEnt;
    hXY_all_subs{kk}=hXY_all;
    sprintf('subject %s is done\n',subname);drawnow;
    save(sprintf('Necessity_all5_%d',kk));
end
    save Necessity5 


N=0*Necessity{1};
for jj=1:length(snames)
    Ne(:,:,jj)=abs(Necessity{jj});
end
N=N/length(snames);
figure;imagesc(N);
figure;imagesc(N-N');
NN=trimmean(Ne,10,3);
figure;imagesc(abs(NN-NN'));

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((Ne(jj,kk,:)-Ne(kk,jj,:)));
    end
end

figure;imagesc((NN-NN')>0);


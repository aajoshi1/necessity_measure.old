function [necessity, joint, marginal, conditional, integrand] = bst_necessity(X, Y, hX, hY, nBins, support)

%% Joint distribution
hX=max(hX,0.0039);hY=max(hY,0.0039);
[joint, bins] = bst_pdf([X; Y], [hX; hY], [-eps 1+eps; -eps 1+eps], nBins);
x = bins(1, :); y = bins(2, :);
dx = mean(diff(x)); dy =  mean(diff(y));

%% Marginal distribution of source
marginal = bst_pdf(Y, hY, [0 1], nBins);

%% Conditional distribution of sink given source
conditional = zeros(nBins, nBins);
replicated = repmat(marginal, nBins, 1);
if ~exist('support','var')
    support=joint>1e-3*max(joint(:));
end
conditional(support) = joint(support) ./ replicated(support);
conditional(isinf(conditional)) = 0;

%% Necessity
integrand = zeros(nBins, nBins);
for m = 1:nBins
  for n = 1:nBins
    if support(m,n) && conditional(m,n) ~= 0 && ~isnan(conditional(m,n)) && ~isinf(conditional(m,n))
      integrand(m, n) = x(m) * (2 * y(n) - 1) * log(conditional(m,n));
    end
  end
end
necessity = sum(sum(integrand)) * dx * dy;

end


%% ================================================================= KERNEL DENSITY ESTIMATION =================================================================

function [density, bins] = bst_pdf(samples, h, ranges, nBins)

%% Setup
nSignals = size(samples, 1); nSamples = size(samples, 2);
K = @(x) exp(- x.^2 / 2) / sqrt(2 * pi);

% Locations of centers of bins in each dimension
infinitesimals = (ranges(:, 2) - ranges(:, 1)) / nBins;
bins = (ranges(:,1) + infinitesimals/2) * ones(1, nBins) + infinitesimals * (0:(nBins-1));

%% Density estimation

if nSignals == 1 % Scalar-valued (univariate) random variable

  density = zeros(1, nBins);
  for idxSample = 1:nSamples
    density = density + K((bins - samples(idxSample)) / h) / h; % Add new sample to joint distribution
  end
  
else % Vector-valued (multivariate) random variable

  density = zeros(nBins * ones(1, nSignals));
  
  for idxSample = 1:nSamples

    next = ones(nBins * ones(1, nSignals));
    for idxSignal = 1:nSignals
      next = next .* ...
        repmat( ...
          reshape( ...
            K((bins(idxSignal, :) - samples(idxSignal, idxSample)) / h(idxSignal)) / h(idxSignal), ... % Kernel is calculated and ...
            [ones(1, idxSignal-1) nBins ones(1, nSignals-idxSignal)] ... % ... placed in correct dimension (dimension idxSignal) ...
          ), ...
          [ones(1, idxSignal-1)*nBins 1 nBins*ones(1, nSignals-idxSignal)] ... % ... and then replicated along all dimensions.
        );
    end

    density = density + next; % Add new sample to joint distribution

  end

end

%% Normalize at end to get total probability density 1
density = density / nSamples;

end

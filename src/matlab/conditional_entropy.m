%||AUM|| ||Shree Ganeshaya Namaha||
function [CE,hXZ,EX,EZ,CE2]=conditional_entropy(X,Z)
NB=200;%eps=.1;
%X=X+rand(size(X))*eps;
%Z=Z+rand(size(Z))*eps;
%xedges=linspace(min(X(:)-eps),max(X(:)+eps),NB+1);
%zedges=linspace(min(Z(:)-eps),max(Z(:)+eps),NB+1);

xedges=linspace(0-eps,1+eps,NB+1);
zedges=linspace(0-eps,1+eps,NB+1);

for jj=1:NB
    xc(jj)=.5*(xedges(jj)+xedges(jj+1));
    zc(jj)=.5*(zedges(jj)+zedges(jj+1));
end

[hX]=histc(X,xedges);hX(end)=[];
[hZ]=histc(Z,zedges);hZ(end)=[];
hX=hX+NB*eps;hZ=hZ+NB*eps;
%hXZ=hist2(X,Z,xedges,zedges);%hXZ(:,end)=[];hXZ(end,:)=[];
%hXZ=hXZ+eps;
dx = xedges(2)-xedges(1);
dz = zedges(2)-zedges(1);
hZ=hZ/(dz*sum(hZ(:)));hX=hX/(dx*sum(hX(:)));

hXZ = mutual_histogram_parzen_variable_size_multithread_double(X, Z, xedges(1), xedges(end), NB, round(NB/3), 4);
hXZ=hXZ';% transpose in hXZ is because the mutual_histogram function gives transposed output

hXZ=hXZ/(dx*dz*sum(hXZ(:)));
hXZ=hXZ+eps;
hXZ=hXZ*1/sum(hXZ(:));
hX=sum(hXZ,2);hZ=sum(hXZ,1);

CE=sum(sum(hXZ.*log2(repmat(hX,1,NB)./hXZ)));
EX=-sum(hX.*log2(hX));
EZ=-sum(hZ.*log2(hZ));

CE2=max(max((hXZ./repmat(hX,1,NB)).*log2(repmat(hX,1,NB)./hXZ)));

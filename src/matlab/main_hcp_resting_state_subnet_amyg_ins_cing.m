%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
ll=dir('D:\hcp\');
 
load vrest_roiwise3_subs
load labs
load v
%labs_id=[3,4,66,139,25,98];
labs_id=[3,66,25];

%labs=[5,6,1048,1048+75,66,139];
%5=AMYGDALA_LEFT
%6=AMYGDALA_RIGHT
%1048=ANTERIOS INSULAR CORTEX Left
%1048+75=ANTERIOS INSULAR CORTEX Right
%1006=G_and_S_cingul-Ant Left
%1006+75=G_and_S_cingul-Ant Right
%20=THALAMUS_LEFT
%21=THALAMUS_RIGHT
%1046=S_central Left
%1046+75=S_central Right
%1028  G_postcentral Left                   
%1028+75  G_postcentral Right 
%1029  G_precentral Left         
%1029+75  G_precentral Right       


kk=0;
vrest_roiwise3_all=vrest_roiwise3;

for kk1=1:length(vrest_roiwise3_all)
    kk=kk1;
    vrest_roiwise3=vrest_roiwise3_all{kk};
    tic
    for l1=1:length(labs_id)
        vrest_roiwise3_l1=vrest_roiwise3(labs_id(l1),:);
        parfor l2=1:length(labs_id)
           
            N(l1,l2)=necessity_empirical(vrest_roiwise3_l1,vrest_roiwise3(labs_id(l2),:));
            [N_XY, N_YX, hXY]=condest_fast(vrest_roiwise3_l1,vrest_roiwise3(labs_id(l2),:));            
            cond=0;
            if abs(N_XY)<abs(N_YX)
                if N_YX>0
                    %fprintf('Increase in X implies increase in Y: X==>Y\n');    
                    cond=1;
                else
                    %fprintf('Increase in X implies decrease in Y: X==>Y\n');    
                    cond=2;
                end
            else
                if N_XY>0
                    %fprintf('Increase in Y implies increase in X: Y==>X\n');   
                    cond=3;
                else
                    %fprintf('Increase in Y implies decrease in X: Y==>X\n');    
                    cond=4;
                end
            end
            CondEnt(l1,l2)=cond;
            N_XY_all(l1,l2)=N_XY;
            hXY_all{l1,l2}=hXY;
            [C_XY]=igci(vrest_roiwise3_l1,vrest_roiwise3(labs_id(l2),:),1,1);
            igci_C_XY_all(l1,l2)=C_XY;%igci_diff_p(l1,l2)=C_XY_diff_p;
            %l2
        end
      %  l1
      %  toc
    end    
    %figure;imagesc(N);
    Necessity{kk}=N;
    igci_C_XY_all_1{kk}=igci_C_XY_all;
    %igci_diff_p_all{kk}=igci_diff_p;
    N_XY_all_subs{kk}=N_XY_all;
    CondEnt_subs{kk}=CondEnt;
    hXY_all_subs{kk}=hXY_all;
    fprintf('subject %d is done\n',kk);drawnow;
  %  save(sprintf('Necessity_all6_%d',kk));
end
    save Necessity6_subnet_amyg_cing_ins_left


N=0*Necessity{1};
for jj=1:length(Necessity)
    Ne(:,:,jj)=(Necessity{jj});
end
NN=trimmean(Ne,10,3);
figure;imagesc(NN-diag(diag(NN)))


for jj=1:length(N_XY_all_subs)
    ENe(:,:,jj)=(N_XY_all_subs{jj});
end
ENN=trimmean(ENe,10,3);
figure;imagesc(ENN-diag(diag(ENN)))


for jj=1:length(igci_C_XY_all_1)
    ENeigci(:,:,jj)=(igci_C_XY_all_1{jj});
end
ENNigci=trimmean(ENeigci,10,3);
figure;imagesc(ENNigci-diag(diag(ENNigci)))


for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((ENeigci(jj,kk,:)));
    end
end

figure;imagesc(NNt.*(NNp<0.01));


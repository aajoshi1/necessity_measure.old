%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
load vrest_roiwise3_subs
vrest_roiwise3_all=vrest_roiwise3;

load labs
%parfor 4
kk=0;
for kk1=length(vrest_roiwise3_all):-1:1
  kk=kk1;  
    vrest_roiwise3=vrest_roiwise3_all{kk1};
    % vrest_roiwise3(:)=vrest_roiwise3(randperm(length(vrest_roiwise3(:))));
    tic
    for l1=1:length(labs)
        vrest_roiwise3_l1=vrest_roiwise3(l1,:);
        parfor l2=1:length(labs)
            N(l1,l2)=necessity_empirical(vrest_roiwise3_l1,vrest_roiwise3(l2,:));
        end
        l1
        toc
    end    
   % figure;imagesc(N);
    Necessity{kk}=N;
    sprintf('subject %d is done\n',kk);
   %drawnow;
    save(sprintf('Necessity_only5_%d',kk));
end
    save Necessity5_only 


N=0*Necessity{1};
for jj=1:length(snames)
    Ne(:,:,jj)=abs(Necessity{jj});
end
N=N/length(snames);
figure;imagesc(N);
figure;imagesc(N-N');
NN=trimmean(Ne,10,3);
figure;imagesc(abs(NN-NN'));

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((Ne(jj,kk,:)-Ne(kk,jj,:)));
    end
end

figure;imagesc((NN-NN')>0);


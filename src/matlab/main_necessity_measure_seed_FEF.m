clc;clear ;close all;
restoredefaultpath;
addpath(genpath('/home/ajoshi/sipi/my_functions'));
addpath(genpath('/home/ajoshi/SpiderOak/necessity_measure/src'));
%addpath(genpath('/home/ajoshi/fcon_1000/matlab_scripts/david_code'));
%addpath(genpath('../../uPC/Graphical-Lasso/'));

flst=dir('/home/ajoshi/fcon_1000/Beijing/sub*');
gunzip('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tp_FEF_3mm.nii.gz')
v_seed=load_nii('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tp_FEF_3mm.nii');
delete('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tp_FEF_3mm.nii');
sn=0;
for subno=1:length(flst)
    
    if ~exist(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'file')
        continue;
    end
    sn=sn+1;
    
    gunzip(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'/tmp/FEF');
    v_rest=load_nii('/tmp/FEF/rest_res2standard.nii');
    delete('/tmp/FEF/rest_res2standard.nii');
    
    %list_labels=unique(atlas_seg2.img(:));list_labels(list_labels==0)=[];list_labels(list_labels==50)=[];
    %r2 = reshape(rest_vol.img,61*73*61,size(rest_vol.img,4));
    sz=size(v_rest.img);
    t_ser = zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));t_ser1=t_ser;
    seed_ind=find(v_seed.img(:)>0);
    %for vox_ind=1:sz(1)*sz(2)*sz(3)
        vrest_vox=zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));
        vrest_seed=zeros(1,size(v_rest.img,4));
        for t=1:size(v_rest.img,4)
            v=v_rest.img(:,:,:,t);
            vrest_vox(:,t)=v(:);
            vrest_seed(t)=mean(v(seed_ind));
        end
     %   vox_ind
        
        %[coeff,score]=princomp(vrest_roi,'econ');
       % t_ser(vox_ind,:)=mean(vox_ind);
        %  t_ser1(lab,:)=zscore(vrest_roi')';
        %   intd = find(atlas_seg2.img==list_labels(lab));
    %end
    %save t_set
    %C = partialcorr(t_ser(1:4:end,:)');figure;imagesc(C);
    t_ser2=zscore(vrest_vox,0,2);t_ser2_seed=zscore(vrest_seed,0,2);%clear t_ser;
    t_ser=(1-normcdf(t_ser2,0,1));t_ser_seed=(1-normcdf(t_ser2_seed,0,1));%t_ser=t_ser';
    
    clear Nvox2seed Nseed2vox;
    
    for jj=1:sz(1)*sz(2)*sz(3)
            Nvox2seed(jj)=necessity_empirical(t_ser(jj,:),t_ser_seed);
            Nseed2vox(jj)=necessity_empirical(t_ser_seed,t_ser(jj,:));
            %    N21(jj,kk)=necessity_empirical(t_ser(kk,:),t_ser(jj,:));
    end
    %figure;
    %imagesc(N12-N12');colorbar;
%    figure;
%    imagesc(N12);colorbar;caxis([-500,500]);
    
    Nvox2seed_sub(:,sn)=Nvox2seed;
    Nseed2vox_sub(:,sn)=Nseed2vox;
    fprintf('%d/%d sub done\n',subno,length(flst));
end

save necessity_msr_seed_FEF Nvox2seed_sub Nseed2vox_sub

aaa=trimmean(Nvox2seed_sub,10,3);
NV=Nvox2seed_sub-repmat(aaa,[1,1,size(Nvox2seed_sub,3)]);
NV=trimmean(NV.^2,10,3);
avar=sqrt(NV);
aaa2=trimmean(Nseed2vox_sub,10,3);
NV2=Nvox2seed_sub-repmat(aaa2,[1,1,size(Nseed2vox_sub,3)]);
NV2=trimmean(NV2.^2,10,3);
avar2=sqrt(NV2);

%avar=std(N12_sub,1,3);
figure;plot(aaa);
figure;plot(aaa2);

figure;plot(abs(aaa-aaa2));
figure;plot(avar);
figure;plot(avar2);



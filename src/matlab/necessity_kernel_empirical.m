%||AUM|| ||Shree Ganeshaya Namaha||
function necessity=necessity_kernel_empirical(X,Y)
%NB=10;%eps=.1;

%xedges=linspace(min(X(:)-eps),max(X(:)+eps),NB+1);
%zedges=linspace(min(Z(:)-eps),max(Z(:)+eps),NB+1);
N=length(X);
hX = 1.06 * std(X) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(X), iqr(X)/1.34) * N^(-1/5)
hY = 1.06 * std(Y) * N^(-1/5); % For multimodal distributions, use 0.9 * min(std(Y), iqr(Y)/1.34) * N^(-1/5)

necessity = bst_necessity(X, Y, hX+eps, hY+eps, 10);
% 
% xedges=linspace(0,1,NB+1);
% zedges=linspace(0,1,NB+1);
% 
% for jj=1:NB
%     xc(jj)=.5*(xedges(jj)+xedges(jj+1));
%     zc(jj)=.5*(zedges(jj)+zedges(jj+1));
% end
% 
% [hX]=histc(X,xedges);hX(end)=[];
% [hZ]=histc(Z,zedges);hZ(end)=[];
% hX=hX+NB*eps;hZ=hZ+NB*eps;
% hXZ=hist2(X,Z,xedges,zedges);hXZ(:,end)=[];hXZ(end,:)=[];
% hXZ=hXZ+eps;
% dx = xedges(2)-xedges(1);
% dz = zedges(2)-zedges(1);
% hZ=hZ/(dz*sum(hZ(:)));hX=hX/(dx*sum(hX(:)));
% hXZ=hXZ/(dx*dz*sum(hXZ(:)));
% 
% It=repmat(zc',1,NB).*(2*repmat(xc,NB,1)-1).*log(hXZ*diag(1./hX))*dx*dz;
% 
% N=sum(It(:))*length(X);

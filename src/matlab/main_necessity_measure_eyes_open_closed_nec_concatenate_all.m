opengl software;
clc;clear ;close all;
restoredefaultpath;
addpath(genpath('/home/ajoshi/sipi/my_functions'));
addpath(genpath('/home/ajoshi/SpiderOak/necessity_measure/src'));
%addpath(genpath('/home/ajoshi/fcon_1000/matlab_scripts/david_code'));
%addpath(genpath('../../uPC/Graphical-Lasso/'));

flst_c=dir('/home/ajoshi/BS003/BS003/BS*c1');
flst_o=dir('/home/ajoshi/BS003/BS003/BS*o1');

%gunzip('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii.gz')
%v_seed=load_nii('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii');
%delete('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii');
sn=0;
    gunzip('/home/ajoshi/fcon_1000/scripts/templates/MNI152_T1_3mm_brain_mask.nii.gz','/tmp/tmpPCC');
    vmsk=load_nii('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    delete('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    vm=vmsk.img;clear vmsk;
%    vm=repmat(vm,[1,1,1,225]);
    maskind=find(vm);clear vm;load sz;
    Nvox2seed_sub=zeros(sz(1)*sz(2)*sz(3),length(flst_c));
    Nseed2vox_sub=zeros(sz(1)*sz(2)*sz(3),length(flst_c));
    vrest_all=[];vseed_all=[];
for subno=1:length(flst_c)
    
    if ~exist(sprintf('/home/ajoshi/BS003/BS003/%s/func/rest_res2standard.nii.gz',flst_c(subno).name),'file')
        continue;
    end
    sn=sn+1
    
    gunzip(sprintf('/home/ajoshi/BS003/BS003/%s/func/rest_res2standard.nii.gz',flst_c(subno).name),'/tmp/tmpPCC');
    v_rest_c=load_nii('/tmp/tmpPCC/rest_res2standard.nii');
    delete('/tmp/tmpPCC/rest_res2standard.nii');
    gunzip(sprintf('/home/ajoshi/BS003/BS003/%s/func/rest_res2standard.nii.gz',flst_o(subno).name),'/tmp/tmpPCC');
    v_rest_o=load_nii('/tmp/tmpPCC/rest_res2standard.nii');
    delete('/tmp/tmpPCC/rest_res2standard.nii');
    
    %list_labels=unique(atlas_seg2.img(:));list_labels(list_labels==0)=[];list_labels(list_labels==50)=[];
    %r2 = reshape(rest_vol.img,61*73*61,size(rest_vol.img,4));
    %sz=size(v_rest.img);
    t_ser = zeros(sz(1)*sz(2)*sz(3),size(v_rest_o.img,4)+size(v_rest_c.img,4));t_ser1=t_ser;
    vrest_vox=zeros(sz(1)*sz(2)*sz(3),size(v_rest_c.img,4)+size(v_rest_o.img,4));
    vrest_seed=zeros(1,size(v_rest_c.img,4)+size(v_rest_o.img,4));
        
        %vrest_vox=v_rest.img(maskind);
        
        for t=1:size(v_rest_c.img,4)
            v1=v_rest_c.img(:,:,:,t);
            vrest_vox(:,t)=v1(:);
            vrest_seed(t)=0;
        end
        
        
        for t=1:size(v_rest_o.img,4)
            v1=v_rest_o.img(:,:,:,t);
            vrest_vox(:,t+size(v_rest_c.img,4))=v1(:);
            vrest_seed(t+size(v_rest_c.img,4))=1;
        end
        
       vrest_all=[vrest_all,vrest_vox];
       vseed_all=[vseed_all,vrest_seed];
       
end       
        
        %   vox_ind
        
        %[coeff,score]=princomp(vrest_roi,'econ');
       % t_ser(vox_ind,:)=mean(vox_ind);
        %  t_ser1(lab,:)=zscore(vrest_roi')';
        %   intd = find(atlas_seg2.img==list_labels(lab));
    %end
    %save t_set
    %C = partialcorr(t_ser(1:4:end,:)');figure;imagesc(C);
    clear N* t* v1* v_* vrest_vox* vrest_seed v_rest_c v_rest_o
    vrest_all=zscore(vrest_all,0,2);%t_ser2_seed=zscore(vrest_seed,0,2);%clear t_ser;
    vrest_all=1-(1-normcdf(vrest_all,0,1));%t_ser_seed=1-(1-normcdf(t_ser2_seed,0,1));%t_ser=t_ser';
    
    clear Nvox2seed Nseed2vox Corrt;
    Nseed2vox=zeros(sz(1)*sz(2)*sz(3),1);
    Nvox2seed=zeros(sz(1)*sz(2)*sz(3),1);
    Corrt=zeros(sz(1)*sz(2)*sz(3),1);
    for jj=1:length(maskind)%sz(1)*sz(2)*sz(3)
            %xlogx1(maskind(jj))=xlogx(t_ser(maskind(jj),:));
            Nseed2vox(maskind(jj))=necessity_empirical(vseed_all,vrest_all(maskind(jj),:));
            Nvox2seed(maskind(jj))=necessity_empirical(vrest_all(maskind(jj),:),vseed_all);
            Corrt(maskind(jj))=corr(vseed_all',vrest_all(maskind(jj),:)');
            %    N21(jj,kk)=necessity_empirical(t_ser(kk,:),t_ser(jj,:));
    end
    %figure;
    %imagesc(N12-N12');colorbar;
%    figure;
%    imagesc(N12);colorbar;caxis([-500,500]);
    


save necessity_msr_eyesopenclosed_concat Corrt Nseed2vox Nvox2seed


aaa=Nvox2seed;%,10,3);
aaa2=Nseed2vox;%,10,3);
aaa3=Corrt;
vol=zeros(61,73,61);
vol(:)=trimmean(aaa,10,2);
%vol(:)=aaa(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_vox2eyesopen_cat.nii');
view_nii(nii2);
%avar=std(N12_sub,1,3);
vol=zeros(61,73,61);
vol(:)=trimmean(aaa2,10,2);
%vol(:)=aaa2(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_eyesopen2vox_cat.nii')
view_nii(nii2);
vol(:)=aaa-aaa2;
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_vox2eyesopen_minus_eyesopen2vox_cat.nii')
view_nii(nii2);

vol(:)=aaa3;
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_msr_Corr_cat.nii')
view_nii(nii2);

li=readdfs(sprintf('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.left.inner.cortex.dfs'));
lp=readdfs(sprintf('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.left.pial.cortex.dfs'));

lm.faces=li.faces;
lm.vertices=.5*(li.vertices+lp.vertices);
%writedfs('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.right.mid.cortex.dfs',lm);
lm=smooth_cortex_fast(lm,.1,6000);
v=load_nii(sprintf('/home/ajoshi/SpiderOak/necessity_measure/src/necessity_msr_Corr_cat.nii'));

v.img(isnan(v.img))=0;v.img(isinf(v.img))=0;
ss=interp3(v.img,lm.vertices(:,2)/3,lm.vertices(:,1)/3,lm.vertices(:,3)/3,'linear');


ss(isnan(ss))=0;ss(isinf(ss))=0;
hh=figure;%caxis([-300,300]);
patch('faces',lm.faces,'vertices',lm.vertices,'edgecolor','none','facevertexcdata',ss,'facecolor','interp');
axis equal; axis off;
view(90,0);zoom(2);colorbar;camlight;material dull;saveas(hh,sprintf('%s_necessity_msr_vox2%s1.png',hemi{h},roi{r},roi{r}));
view(-90,0);colorbar;camlight;material dull;saveas(hh,sprintf('%s_necessity_msr_vox2%s2.png',hemi{h},roi{r},roi{r}));

% 
% figure;plot(aaa);
% figure;plot(aaa2);
% 
% figure;plot(abs(aaa-aaa2));
% figure;plot(avar);
% figure;plot(avar2);
% 
% 

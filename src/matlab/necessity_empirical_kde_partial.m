function N=necessity_empirical_kde_partial(X,Z,W)
NB=100;
xedges=linspace(0-eps,1+eps,NB+1);
zedges=linspace(0-eps,1+eps,NB+1);

for jj=1:NB
    xc(jj)=.5*(xedges(jj)+xedges(jj+1));
    zc(jj)=.5*(zedges(jj)+zedges(jj+1));
end

hXZW=kde([X,Z,W]','rot');
hXZ=condition(hXZW,[3:2+size(W,2)],zeros(length(W),1));
dx = xedges(2)-xedges(1);
dz = zedges(2)-zedges(1);

hX=marginal(hXZ,1);
[X,Z]=meshgrid(xc);
%hZgivenX=condition(hXZ,1,X(:));
%hZgivenX1=evaluate(hZgivenX,Z(:)');
hZgivenX1=evaluate(hXZ,[X(:),Z(:)]')./evaluate(hX,X(:)');
It=repmat(zc',1,NB).*(2*repmat(xc,NB,1)-1).*reshape(log2(hZgivenX1),NB,NB)*dx*dz;

N=sum(It(:));




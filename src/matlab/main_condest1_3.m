%||AUM||
clc;clear all;close all;
addpath('igci');
meta=load('../data/pairmeta.txt');
dt1=meta(:,end);
tic
for dt1=1:length(meta)
    close all;
    if dt1==70
        continue;
    end
    a=load(sprintf('../data/pair%04d.txt',meta(dt1,1)));
    
    X=a(:,meta(dt1,2));Y=a(:,meta(dt1,4));
    
    ind=find(abs(X-trimmean(X,90))>2*std(X));
    ind2=find(abs(Y-trimmean(Y,90))>2*std(Y)); ind=union(ind,ind2);
    X(ind)=[];Y(ind)=[];
    
    X_t=zscore(X',0,2);Y_t=zscore(Y',0,2);%clear t_ser;
    X_p=1-(1-normcdf(X_t,0,1));Y_p=1-(1-normcdf(Y_t,0,1));%t_ser=t_ser';
    
%     XYcov=cov(X_p',Y_p');XYcov=XYcov(1,2);
%    if XYcov<0
%        Y_p=1-Y_p;
%    end
   
    [N_XY, N_YX, N_XY_diff_p,hXY]=condest_pval3(X_p,Y_p);
    
    cond=0;
    
    if abs(N_XY)<abs(N_YX)
        if N_YX>0
            fprintf('Increase in X implies increase in Y: X==>Y\n');    cond=1;
        else
            fprintf('Increase in X implies decrease in Y: X==>Y\n');    cond=2;
        end
    else
        if N_XY>0
            fprintf('Increase in Y implies increase in X: Y==>X\n');    cond=3;
        else
            fprintf('Increase in Y implies decrease in X: Y==>X\n');    cond=4;
        end
    end
 
    h=figure;plot(X_p,Y_p,'.');
    saveas(h,sprintf('pair%04d_scatter_condest.png',meta(dt1,1)));
    h=figure;imagesc(flipud(hXY'));axis off;
    saveas(h,sprintf('pair%04d_jointpdf_condest.png',meta(dt1,1)));
%        covxy(dt1)=XYcov;
    cond1(dt1)=cond;
    pval_nec(dt1)=(N_XY_diff_p)
    dt1=meta(:,end)
    
    save output1_3_condest dt1 pval_nec cond1
end
save output1_3_condest




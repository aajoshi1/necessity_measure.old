%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
ll=dir('D:\hcp\');
 
jj=1;
for kk=1:length(ll)
    if ll(kk).isdir==1 && length(ll(kk).name)==6
        snames{jj}=ll(kk).name;jj=jj+1;
    end
end

kk=0;
for kk1=1:length(snames)
    subname=snames{kk1};
    if ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname),'file') || ~exist(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname),'file')
        continue;
    end
    kk=kk+1;
    v=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\Results\\rfMRI_REST1_LR\\rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii',subname));
    vs=ft_read_cifti(sprintf('D:\\hcp\\%s\\MNINonLinear\\fsaverage_LR32k\\%s.aparc.a2009s.32k_fs_LR.dlabel.nii',subname,subname));
    
    v.brainstructure(1:length(vs.aparc_a2009s))=1000+vs.aparc_a2009s;  %adding 1000 to distinguish surface labels from volume labels
    
    
    labs=unique(v.brainstructure(~isnan(v.brainstructure)));
    labs=setdiff(labs,[1000,1076]);
    
    for l=1:length(labs)
        l_ind=find(v.brainstructure==labs(l));
        for t=1:size(v.dtseries,2)
            v1=v.dtseries(:,t);
            vrest_roiwise(l,t)=mean(v1(l_ind));
        end
        %vrest_roiwise
        %l
    end
    vrest_roiwise_allsub{kk}=vrest_roiwise-repmat(mean(vrest_roiwise,2),1,size(vrest_roiwise,2));
    vrest_roiwise2=vrest_roiwise_allsub{kk}/std(vrest_roiwise_allsub{kk}(:));
    vrest_roiwise3{kk}=1-(1-normcdf(vrest_roiwise2,0,1));
    % vrest_roiwise3(:)=vrest_roiwise3(randperm(length(vrest_roiwise3(:))));
kk1
end

save vrest_roiwise_allsubs_signal vrest_roiwise3 vrest_roiwise_allsub

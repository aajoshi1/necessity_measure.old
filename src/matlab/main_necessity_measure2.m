clc;clear ;close all;
restoredefaultpath;
addpath(genpath('/home/ajoshi/sipi/my_functions'));
addpath(genpath('/home/ajoshi/SpiderOak/necessity_measure/src'));
%addpath(genpath('/home/ajoshi/fcon_1000/matlab_scripts/david_code'));
%addpath(genpath('../../uPC/Graphical-Lasso/'));

flst=dir('/home/ajoshi/fcon_1000/Beijing/sub*');
sn=0;
for subno=1:length(flst)
    
    if ~exist(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'file')
        continue;
    end
    sn=sn+1;
    
    gunzip(sprintf('/home/ajoshi/fcon_1000/Beijing/%s/func/rest_res2standard.nii.gz',flst(subno).name),'/tmp');
    rest_vol=load_nii('/tmp/rest_res2standard.nii');
    delete('/tmp/rest_res2standard.nii');
    
    
    
    atlas_seg=load_nii('/home/ajoshi/fcon_1000/matlab_scripts/HarvardOxford-cort-maxprob-thr25-3mm.nii');
    
    
    atlas_seg2 = atlas_seg;
    atlas_seg2.img(32:end,:,:) = atlas_seg2.img(32:end,:,:)+50;
    
    list_labels=unique(atlas_seg2.img(:));list_labels(list_labels==0)=[];list_labels(list_labels==50)=[];
    %r2 = reshape(rest_vol.img,61*73*61,size(rest_vol.img,4));
    t_ser = zeros(length(list_labels),size(rest_vol.img,4));t_ser1=t_ser;
    for lab=1:length(list_labels)
        roi_ind=find(atlas_seg2.img==list_labels(lab));
        vrest_roi=zeros(length(roi_ind(:)),size(rest_vol.img,4));
        for t=1:size(rest_vol.img,4)
            v=rest_vol.img(:,:,:,t);
            vrest_roi(:,t)=v(roi_ind);
        end
        %[coeff,score]=princomp(vrest_roi,'econ');
        t_ser(lab,:)=mean(vrest_roi);
        %  t_ser1(lab,:)=zscore(vrest_roi')';
        %   intd = find(atlas_seg2.img==list_labels(lab));
    end
    %save t_set
    %C = partialcorr(t_ser(1:4:end,:)');figure;imagesc(C);
    t_ser2=zscore(t_ser');clear t_ser;
    t_ser=(1-normcdf(t_ser2,0,1));t_ser=t_ser';
    for jj=1:length(list_labels)
        for kk=1:length(list_labels)
            N12(jj,kk)=necessity_empirical(t_ser(jj,:),t_ser(kk,:));
            %    N21(jj,kk)=necessity_empirical(t_ser(kk,:),t_ser(jj,:));
        end
    end
    %figure;
    %imagesc(N12-N12');colorbar;
    figure;
    imagesc(N12);colorbar;caxis([-500,500]);
    
    N12_sub(:,:,sn)=N12;
    fprintf('%d/%d sub done\n',subno,length(flst));
end

save necessity_msr N12_sub

aaa=trimmean(N12_sub,10,3);
NV=N12_sub-repmat(aaa,[1,1,size(N12_sub,3)]);
NV=trimmean(NV.^2,10,3);
avar=sqrt(NV);
%avar=std(N12_sub,1,3);
figure;imagesc(aaa);
figure;imagesc(abs(aaa'-aaa));
figure;imagesc(avar);



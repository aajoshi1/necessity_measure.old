%||AUM||
%||Shree Ganeshaya Namaha||
function [N_XY, N_YX, N_XY_diff_p,hXY]= condest_pval4(X_p,Y_p)

[N_XY,hXY]=conditional_entropy(X_p,Y_p);
    N_YX=conditional_entropy(Y_p,X_p);
    
    N_XY_null=zeros(1000,1);N_YX_null=zeros(1000,1);
    for jj=1:1000
        Vec=[X_p',Y_p'];
 aa=randperm(length(X_p));
 ind1=sub2ind(size(Vec),aa',ones(length(aa),1));
        ind2=sub2ind(size(Vec),[1:length(aa)]',2*ones(length(aa),1));
        Vecp=[Vec(ind1),Vec(ind2)];
        N_XY_null(jj)=conditional_entropy(Vecp(:,1),Vecp(:,2));
        N_YX_null(jj)=conditional_entropy(Vecp(:,2),Vecp(:,1));
    end
    
    N_XY_diff_p=sum((N_XY_null-N_YX_null)>(N_XY-N_YX))/1000;
    N_YX_diff_p=sum((N_YX_null-N_XY_null)>(N_YX-N_XY))/1000;

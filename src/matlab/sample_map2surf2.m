% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 
opengl software;
clc;close all;clear% all;
restoredefaultpath;
%addpath(genpath('../src/MEX_Files'));
addpath(genpath('/home/ajoshi/svreg-matlab/src'));
%addpath(genpath('../dev'));
hemi={'left','right'};
roi={'PCC','IPS'};
for h=1:2
    for r=1:2
        
li=readdfs(sprintf('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.%s.inner.cortex.dfs',hemi{h}));
lp=readdfs(sprintf('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.%s.pial.cortex.dfs',hemi{h}));

lm.faces=li.faces;
lm.vertices=.5*(li.vertices+lp.vertices);
%writedfs('/home/ajoshi/SpiderOak/necessity_measure/MNI152/MNI152_T1_1mm.right.mid.cortex.dfs',lm);
lm=smooth_cortex_fast(lm,.1,6000);
v=load_nii(sprintf('/home/ajoshi/SpiderOak/necessity_measure/src/necessity_msr_vox2%s.nii',roi{r}));
v.img(isnan(v.img))=0;v.img(isinf(v.img))=0;%necessity_msr_vox2PCC_minus_PCC2vox_sub1
ss=interp3(v.img,lm.vertices(:,2)/3,lm.vertices(:,1)/3,lm.vertices(:,3)/3,'linear');


ss(isnan(ss))=0;ss(isinf(ss))=0;
hh=figure;%caxis([-300,300]);
patch('faces',lm.faces,'vertices',lm.vertices,'edgecolor','none','facevertexcdata',ss,'facecolor','interp');
axis equal; axis off;
view(90,0);zoom(2);colorbar;camlight;material dull;
saveas(hh,sprintf('%s_necessity_msr_vox2%s1.png',hemi{h},roi{r}));
view(-90,0);colorbar;camlight;material dull;
saveas(hh,sprintf('%s_necessity_msr_vox2%s2.png',hemi{h},roi{r}));

    end
end
%||AUM||
%||Shree Ganeshaya Namaha||
function [C_XY,C_XY_diff_p]= igci_pval(X_p,Y_p)

[C_XY]=igci(X_p,Y_p,1,1);
    
    C_XY_null=zeros(1000,1);
    for jj=1:1000
        Vec=[X_p',Y_p'];
        aa=1+round(rand(length(X_p),1));
        ind1=sub2ind(size(Vec),[1:length(aa)]',aa);
        ind2=sub2ind(size(Vec),[1:length(aa)]',3-aa);
        Vecp=[Vec(ind1),Vec(ind2)];
        C_XY_null(jj)=igci(Vecp(:,1),Vecp(:,2),1,1);
    end
    toc
    C_XY_diff_p=sum(abs(C_XY_null)>abs(C_XY))/1000;

    
%||AUM||
%||Shree Ganeshaya Namaha||
function [N_XY, N_YX, hXY]= condest_fast_kde(X,Y)

%[N_XY,hXY]=conditional_entropy_kde(X_p,Y_p);
%    N_YX=conditional_entropy_kde(Y_p,X_p);
    
% 
% hXY=kde([X,Y]','rot');
% hX=kde([X]','rot');
% hY=kde([Y]','rot');
% hYgivenX=condition(hXY,1,X(:));
% hXgivenY=condition(hXY,2,Y(:));
% 
% N_XY=log2(entropy(hY)/entropy(hYgivenX));
% 
% N_YX=log2(entropy(hX)/entropy(hXgivenY));
% 
hXY=kde([X,Y]','rot');
hX=kde([X]','rot');
hY=kde([Y]','rot');
%hYgivenX=condition(hXY,1,X(:));
entropyY=entropy(hY);
entropyX=entropy(hX);
entropyXY=entropy(hXY);

N_XY=log2(entropyY/(entropyXY-entropyX));
N_YX=log2(entropyX/(entropyXY-entropyY));
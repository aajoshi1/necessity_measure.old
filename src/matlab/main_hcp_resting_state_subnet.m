%||AUM||
clc; clear all; close all;
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\necessity_measure\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\src'));
addpath(genpath('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\dev\thickness\'));
%vpvc_lab_5min=load_nii_z('C:\Users\ajoshi\Dropbox\3MPRAGE-samebrain_testing\newscanner_mprageagisos\7783AD_AH_newscanner__mpragesagisos.pvc.label.nii.gz');
ll=dir('D:\hcp\');
 
load vrest_roiwise3_subs
load labs
load v
labs_id=[3,4,66,139,25,98];
%labs=[5,6,1048,1048+75,66,139];
%5=AMYGDALA_LEFT
%6=AMYGDALA_RIGHT
%1048=ANTERIOS INSULAR CORTEX Left
%1048+75=ANTERIOS INSULAR CORTEX Right
%1006=G_and_S_cingul-Ant Left
%1006+75=G_and_S_cingul-Ant Right

%surf labels 24  G_orbital                       220  60  20    0
%surf labels 48  S_circular_insula_ant           221  60 140    0
  
kk=0;
vrest_roiwise3_all=vrest_roiwise3;

for kk1=1:length(vrest_roiwise3_all)
    kk=kk1;
    vrest_roiwise3=vrest_roiwise3_all{kk};
    tic
    for l1=1:length(labs_id)
        vrest_roiwise3_l1=vrest_roiwise3(l1,:);
        parfor l2=1:length(labs_id)
           
            N(l1,l2)=necessity_empirical(vrest_roiwise3_l1,vrest_roiwise3(l2,:));
            [N_XY, N_YX, hXY]=condest_fast(vrest_roiwise3_l1,vrest_roiwise3(l2,:));            
            cond=0;
            if abs(N_XY)<abs(N_YX)
                if N_YX>0
                    %fprintf('Increase in X implies increase in Y: X==>Y\n');    
                    cond=1;
                else
                    %fprintf('Increase in X implies decrease in Y: X==>Y\n');    
                    cond=2;
                end
            else
                if N_XY>0
                    %fprintf('Increase in Y implies increase in X: Y==>X\n');   
                    cond=3;
                else
                    %fprintf('Increase in Y implies decrease in X: Y==>X\n');    
                    cond=4;
                end
            end
            CondEnt(l1,l2)=cond;
            %N_XY_diff_all(l1,l2)=N_XY_diff_p;
            hXY_all{l1,l2}=hXY;
            [C_XY]=igci(vrest_roiwise3_l1,vrest_roiwise3(l2,:),1,1);
            igci_C_XY_all(l1,l2)=C_XY;%igci_diff_p(l1,l2)=C_XY_diff_p;
            %l2
        end
      %  l1
      %  toc
    end    
    %figure;imagesc(N);
    Necessity{kk}=N;
    igci_C_XY_all_1{kk}=igci_C_XY_all;
    %igci_diff_p_all{kk}=igci_diff_p;
    %N_XY_diff_all_subs{kk}=N_XY_diff_all;
    CondEnt_subs{kk}=CondEnt;
    hXY_all_subs{kk}=hXY_all;
    fprintf('subject %d is done\n',kk);drawnow;
  %  save(sprintf('Necessity_all6_%d',kk));
end
    save Necessity6_subnet 


N=0*Necessity{1};
for jj=1:length(Necessity)
    Ne(:,:,jj)=abs(Necessity{jj});
end
N=N/length(Necessity);
figure;imagesc(N);
figure;imagesc(N-N');
NN=trimmean(Ne,10,3);
figure;imagesc(abs(NN-NN'));

for kk=1:size(Ne,2)
    for jj=1:size(Ne,1)
        [NNt(jj,kk),NNp(jj,kk)]=ttest((Ne(jj,kk,:)));
    end
end

figure;imagesc(NNt.*(NNp<0.01));


clc;clear ;close all;
restoredefaultpath;
addpath(genpath('/home/ajoshi/sipi/my_functions'));
addpath(genpath('/home/ajoshi/SpiderOak/necessity_measure/src'));
%addpath(genpath('/home/ajoshi/fcon_1000/matlab_scripts/david_code'));
%addpath(genpath('../../uPC/Graphical-Lasso/'));

flst=dir('/home/ajoshi/BS003/BS003/BS*c1');
gunzip('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii.gz')
v_seed=load_nii('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii');
delete('/home/ajoshi/fcon_1000/scripts/seeds/Fox_tn_PCC_3mm.nii');
sn=0;
    gunzip('/home/ajoshi/fcon_1000/scripts/templates/MNI152_T1_3mm_brain_mask.nii.gz','/tmp/tmpPCC');
    vmsk=load_nii('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    delete('/tmp/tmpPCC/MNI152_T1_3mm_brain_mask.nii');
    vm=vmsk.img;clear vmsk;
%    vm=repmat(vm,[1,1,1,225]);
    maskind=find(vm);clear vm;load sz;
    Nvox2seed_sub=zeros(sz(1)*sz(2)*sz(3),length(flst));
    Nseed2vox_sub=zeros(sz(1)*sz(2)*sz(3),length(flst));
    
for subno=1:length(flst)
    
    if ~exist(sprintf('/home/ajoshi/BS003/BS003/%s/func/rest_res2standard.nii.gz',flst(subno).name),'file')
        continue;
    end
    sn=sn+1;
    
    gunzip(sprintf('/home/ajoshi/BS003/BS003/%s/func/rest_res2standard.nii.gz',flst(subno).name),'/tmp/tmpPCC');
    v_rest=load_nii('/tmp/tmpPCC/rest_res2standard.nii');
    delete('/tmp/tmpPCC/rest_res2standard.nii');
    
    %list_labels=unique(atlas_seg2.img(:));list_labels(list_labels==0)=[];list_labels(list_labels==50)=[];
    %r2 = reshape(rest_vol.img,61*73*61,size(rest_vol.img,4));
    %sz=size(v_rest.img);
    t_ser = zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));t_ser1=t_ser;
    seed_ind=find(v_seed.img(:)>0);
    %for vox_ind=1:sz(1)*sz(2)*sz(3)
        vrest_vox=zeros(sz(1)*sz(2)*sz(3),size(v_rest.img,4));
        vrest_seed=zeros(1,size(v_rest.img,4));
        
        %vrest_vox=v_rest.img(maskind);
        
        for t=1:size(v_rest.img,4)
            v1=v_rest.img(:,:,:,t);
            vrest_vox(:,t)=v1(:);
            vrest_seed(t)=mean(v1(seed_ind));
        end
     %   vox_ind
        
        %[coeff,score]=princomp(vrest_roi,'econ');
       % t_ser(vox_ind,:)=mean(vox_ind);
        %  t_ser1(lab,:)=zscore(vrest_roi')';
        %   intd = find(atlas_seg2.img==list_labels(lab));
    %end
    %save t_set
    %C = partialcorr(t_ser(1:4:end,:)');figure;imagesc(C);
    t_ser2=zscore(vrest_vox,0,2);t_ser2_seed=zscore(vrest_seed,0,2);%clear t_ser;
    t_ser=1-(1-normcdf(t_ser2,0,1));t_ser_seed=1-(1-normcdf(t_ser2_seed,0,1));%t_ser=t_ser';
    
    clear Nvox2seed Nseed2vox Corrt;
    xlogx1=zeros(sz(1)*sz(2)*sz(3),1);
    %Nseed2vox=zeros(sz(1)*sz(2)*sz(3),1);
    %Corrt=zeros(sz(1)*sz(2)*sz(3),1);
    for jj=1:length(maskind)%sz(1)*sz(2)*sz(3)
            xlogx1(maskind(jj))=xlogx(t_ser(maskind(jj),:));
            %Nseed2vox(maskind(jj))=necessity_empirical(t_ser_seed,t_ser(maskind(jj),:));
     %       Corrt(maskind(jj))=corr(t_ser_seed',t_ser(maskind(jj),:)');
            %    N21(jj,kk)=necessity_empirical(t_ser(kk,:),t_ser(jj,:));
    end
    %figure;
    %imagesc(N12-N12');colorbar;
%    figure;
%    imagesc(N12);colorbar;caxis([-500,500]);
    
    xlogx1sub(:,sn)=xlogx1;
    %Nseed2vox_sub(:,sn)=Nseed2vox;
    %Corr(:,sn)=Corrt;
    fprintf('%d/%d sub done\n',subno,length(flst));
end

save necessity_msr_BS003c1xlogx xlogx1sub

load necessity_msr_BS003c1xlogx

xlogx1subc1=xlogx1sub;
load necessity_msr_BS003o1xlogx

xlogx1subo1=xlogx1sub;


necessityofeyesopen=xlogx1subo1-xlogx1subc1;

vol=zeros(61,73,61);
vol(:)=trimmean(necessityofeyesopen,10,2);
%vol(:)=aaa2(:,2);
nii2=make_nii(vol,[3 3 3]);
save_nii(nii2,'necessity_eyesopen.nii')
view_nii(nii2);
